/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        'login': "url('./src/assets/images/hamburger-login.svg')",
      },
      boxShadow: {
        '3xl': '3px 3px 10px rgba(74, 144, 226, 0.19)',
    },
  },
},
  plugins: [],
};
