import React from 'react'

import Register from './containers/Register'
import Login from './containers/Login'

function App() {
 
  return (
    <div className='bg-slate-600 h-screen'>
      {/* <Register /> */}
      <Login />

    </div>
  )
}

export default App
