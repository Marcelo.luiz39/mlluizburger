import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

import BackgroundImage from "../../assets/images/hamburger-login.svg";
import Logo from "../../assets/logo.svg";
import api from "../../services/api";

function Login() {
  const schema = Yup.object().shape({
    email: Yup.string()
      .email("Digite um email valido!")
      .required("O email é obrigatório!"),
    password: Yup.string()
      .required("A senha é obrigatória!")
      .min(8, "No mínimo 8 caracteres!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (dataClient) => {
    try {
      const response = await api.post("sessions", dataClient);
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const [errorPassword, setErrorPassword] = React.useState({
    errors: false,
    message: "",
  });

  return (
    <div className="flex h-screen w-full flex-col md:flex-row">
      <div
        className="md:w-1/1 hidden h-1/3 w-full bg-cover bg-center md:block md:h-full"
        style={{ backgroundImage: `url(${BackgroundImage})` }}
      ></div>

      <div className="flex h-2/3 w-full flex-col items-center justify-center md:h-full md:w-1/2">
        <div className="mt-20 md:mt-4 lg:mb-40">
          <img src={Logo} alt="logo" />
        </div>
        <form noValidate onSubmit={handleSubmit(onSubmit)}>
          <h2 className="mb-5 mt-5 text-center text-2xl font-medium text-white">
            Login
          </h2>
          <div className="mb-5">
        <label
          htmlFor="email"
          className="mb-1 text-sm font-medium text-white"
        >
          Email
        </label>
        <input
          type="email"
          className={`border ${
            errors.email ? "border-red-700" : "border-none"
          } rounded-md bg-white p-2 w-full shadow-3xl focus:outline-none`}
          id="email"
          placeholder="Digite seu email"
          {...register("email", { required: true })}
        />
        {errors.email ? (
          <p className="text-sm font-medium text-red-600">{errors.email.message}</p>
        ) :( <p className="pb-5"></p>)}
      </div>
          <div className="mb-5">
            <label
              className="text-sm font-medium text-white"
              htmlFor="password"
            >
              Password
            </label>
            <input
              className={`border ${
                errors.password ? "border-red-700" : "border-none"
              } rounded-md bg-white p-2 w-full shadow-3xl focus:outline-none`}
              type="password"
              {...register("password")}
              error={errors.password?.message}
              placeholder="Digite sua senha"
            />
             {errors.password ? (
          <p className="text-sm font-medium text-red-600">{errors.password.message}</p>
        ) :( <p className="pb-5"></p>)}
            {/* <div>
              {errors.password?.message?.length ? (
                <p className="text-sm font-medium text-red-600">
                  A senha é obrigatória!
                </p>
              ) : (
                <p className="pb-5"></p>
              )}
            </div> */}
          </div>
          <div className="my-6">
            <button
              className="mt-16 flex h-8 w-44 items-center justify-center rounded-2xl bg-fuchsia-600 py-2 px-4 font-bold text-white hover:bg-fuchsia-500"
              type="submit"
            >
              Entrar
            </button>
          </div>
          <div className="mt-5 text-white">
            Não tem uma conta ?{" "}
            <a className="underline" href="#">
              Cadastre-se
            </a>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Login;
