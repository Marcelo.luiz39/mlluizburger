import { yupResolver } from "@hookform/resolvers/yup";
import React from "react";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

import Logo from "../../assets/logo.svg";
import api from "../../services/api";

function login() {
  const schema = Yup.object().shape({
    name: Yup.string("O  seu nome é obrigatório!").required(
      "O nome é obrigatório!"
    ),
    email: Yup.string()
      .email("Digite um email valido!")
      .required("O email é obrigatório!"),
    password: Yup.string()
      .required("A senha é obrigatória!")
      .min(8, "No mínimo 8 caracteres!"),
    confirmPassword: Yup.string()
      .required("A senha é obrigatória!")
      .oneOf([Yup.ref("password")], "As senhas devem ser iguais!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (dataClient) => {
    try {
      const response = await api.post("users", dataClient);
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <img src={Logo} alt="logo" />

      <h1>Cadastre-se</h1>

      <form noValidate onSubmit={handleSubmit(onSubmit)}>
        <label>
          Nome
          <input
            type="text"
            {...register("name")}
            error={errors.name?.message}
            placeholder="Digite seu nome"
          />
        </label>
        <label>
          Email
          <input
            type="email"
            {...register("email")}
            error={errors.email?.message}
            placeholder="Digite seu email"
          />
        </label>
        <label>
          Senha
          <input
            type="password"
            {...register("password")}
            error={errors.password?.message}
            placeholder="Digite sua senha"
          />
        </label>
        <label>
          Confirmar Senha
          <input
            type="password"
            {...register("confirmPassword")}
            error={errors.confirmPassword?.message}
            placeholder="Digite sua senha"
          />
        </label>
        <button type="submit" style={{ marginBottom: 25, marginTop: 25 }}>
          Cadastrar
        </button>
        Já possui conta ? <a href="#">Entrar</a>
      </form>
    </div>
  );
}

export default login;
